//{ Driver Code Starts
//Initial Template for Java

import java.util.*;
import java.io.*;
import java.lang.*;

class GFG
{
    public static void main(String args[])throws IOException
    {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(read.readLine());
        
        while(t-- > 0)
        {
            String str[] = read.readLine().trim().split(" ");
            int n = Integer.parseInt(str[0]);
            
            int arr[] = new int[n];
            int dep[] = new int[n];
            
            str = read.readLine().trim().split(" ");
            for(int i = 0; i < n; i++)
              arr[i] = Integer.parseInt(str[i]);
            str = read.readLine().trim().split(" ");
            for(int i = 0; i < n; i++)
                dep[i] = Integer.parseInt(str[i]);
                
            System.out.println(new Solution().findPlatform(arr, dep, n));
        }
    }
    
    
}



// } Driver Code Ends

class Solution {
    //Function to find the minimum number of platforms required at the
    //railway station such that no train waits.
    static int findPlatform(int arr[], int dep[], int n) {
        // Sorting arrival and departure arrays
        Arrays.sort(arr);
        Arrays.sort(dep);

        int platformsNeeded = 1; // Minimum one platform is needed
        int result = 1;
        int i = 1, j = 0;

        // Similar to merge in merge sort to process all events in sorted order
        while (i < n && j < n) {
            // If next event is arrival, increment count of platforms needed
            if (arr[i] <= dep[j]) {
                platformsNeeded++;
                i++;
            } 
            // If next event is departure, decrement count of platforms needed
            else if (arr[i] > dep[j]) {
                platformsNeeded--;
                j++;
            }

            // Update result if needed
            if (platformsNeeded > result)
                result = platformsNeeded;
        }

        return result;
    }
}
